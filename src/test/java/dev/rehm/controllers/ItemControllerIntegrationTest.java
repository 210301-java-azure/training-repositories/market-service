package dev.rehm.controllers;

import dev.rehm.JavalinApp;
import dev.rehm.models.MarketItem;
import kong.unirest.GenericType;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import org.junit.jupiter.api.*;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ItemControllerIntegrationTest {

    private static JavalinApp app = new JavalinApp();

    @BeforeAll
    public static void startService(){
        app.start(7000);
    }

    @AfterAll
    public static void stopService(){
        app.stop();
    }

    @Test
    @Disabled
    public void testGetAllItemsUnauthorized(){
        HttpResponse<String> response = Unirest.get("http://localhost:7000/items").asString();
        assertAll(
                ()->assertEquals( 401,response.getStatus()),
                ()->assertEquals( "Unauthorized",response.getBody()));
    }

    @Test
    @Disabled
    public void testGetAllItemsAuthorized(){
        HttpResponse<List<MarketItem>> response = Unirest.get("http://localhost:7000/items")
                .header("Authorization", "admin-auth-token")
                .asObject(new GenericType<List<MarketItem>>() {});
        assertAll(
                ()->assertEquals(200,response.getStatus()),
                ()->assertTrue(response.getBody().size()>0)
        );
    }

}
