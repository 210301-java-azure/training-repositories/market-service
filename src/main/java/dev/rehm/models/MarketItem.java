package dev.rehm.models;

import java.io.Serializable;
import java.util.Objects;

public class MarketItem implements Serializable {

    private int id;
    private double price;
    private String name;

    public MarketItem(){
        super();
    }

    public MarketItem(int id, double price, String name) {
        this.id = id;
        this.price = price;
        this.name = name;
    }

    public MarketItem(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MarketItem that = (MarketItem) o;
        return id == that.id && Double.compare(that.price, price) == 0 && Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, price, name);
    }

    @Override
    public String toString() {
        return "MarketItem{" +
                "id=" + id +
                ", price=" + price +
                ", name='" + name + '\'' +
                '}';
    }
}
