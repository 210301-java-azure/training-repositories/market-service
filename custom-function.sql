CREATE OR REPLACE FUNCTION create_market_item(item_name character varying, price double precision)
 RETURNS SETOF market_item
 LANGUAGE plpgsql
AS $function$
declare 
	new_id integer := nextval('market_item_id_seq'::regclass);
begin
	insert into market_item values (new_id, item_name, price);
	return query select * from market_item where id = new_id;
end
$function$